package com.doublehuo.mariobros.Sprites.TileObjects;

import com.badlogic.gdx.maps.MapObject;
import com.doublehuo.mariobros.MarioBros;
import com.doublehuo.mariobros.Scenes.Hud;
import com.doublehuo.mariobros.Screens.PlayScreen;
import com.doublehuo.mariobros.Sprites.Mario;

/**
 * Created by DoubleHUO on 6/4/18.
 */

public class Brick extends InteractiveTileObject {
    public Brick(PlayScreen screen, MapObject object){
        super(screen, object);
        fixture.setUserData(this);
        setCategoryFilter(MarioBros.BRICK_BIT);
    }

    @Override
    public void onHeadHit(Mario mario) {
        if(mario.isBig()) {
            setCategoryFilter(MarioBros.DESTROYED_BIT);
            getCell().setTile(null);
            Hud.addScore(200);
            //MarioBros.manager.get("audio/sounds/breakblock.wav", Sound.class).play();
        }
       // MarioBros.manager.get("audio/sounds/bump.wav", Sound.class).play();
    }

}
